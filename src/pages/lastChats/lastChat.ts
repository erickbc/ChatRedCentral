import { ChatGroupViewPage } from './../chatGroup-view/chatGroup-view';
import { Observable } from 'rxjs/Observable';
import { ChatsProvider } from './../../providers/chats-provider/chats-provider';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FirebaseListObservable, AngularFire, FirebaseObjectObservable } from 'angularfire2';
import { UserProvider } from '../../providers/user-provider/user-provider';
import { ChatViewPage } from '../chat-view/chat-view';

@Component({
    templateUrl: 'lastChat.html'
})
export class LastChatPage {
    users:FirebaseListObservable<any[]>;
    issues:Observable<any[]>;
    userObservable:FirebaseObjectObservable<any>;
    //uid:string;

    uid:string;
    nameUser:string;
    kindUser = 2001
      constructor(
        public chatsProvider: ChatsProvider,
        public userProvider: UserProvider,
        public af:AngularFire,
        public nav: NavController
      ) {
          //Obtiene los issues donde esta cada usuario, los mas recientes donde
          //ha participado , de tal manera que obtenemos el ultimo chat
          this.kindUser = this.userProvider.kindUser
          this.issues = this.chatsProvider.getIssuesUserNow().map(
            groups => {
            return groups.map(group => {return group;});
          });
      }


      openChat(issue) {
        console.log("lastChat.ts - openChat");
        console.log();
        let param = {
          keyGroup:issue.$key,
          nameGroup:issue.nameGroup,
          nameIssue:issue.nameIssue,
          idGroup:issue.idGroup


        }
        this.nav.push(ChatGroupViewPage,param);

      }




}
