import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TabsPage } from '../tabs/tabs';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { validateEmail } from '../../validators/email';
import { AuthProvider } from '../../providers/auth-provider/auth-provider';
import { UserProvider } from '../../providers/user-provider/user-provider';
import { UtilProvider } from '../../providers/utils';
import { AngularFire, AngularFireModule, AuthProviders, AuthMethods,FirebaseAuth } from 'angularfire2';
import * as firebase from 'firebase';



@Component({
  selector: 'page-home',
  templateUrl: 'view-editUser.html'
})
export class ViewEditUsuario {
    loginForm:any;
    token:any;
    kindUser: any = 'normal';
    nickname:string = "";
    user:any;


    firebaseConfig = {
      apiKey: "AIzaSyAzpkMEMv3xmVA8f3wYdzEz86dfEFhV9eY",
        authDomain: "chattest-2f364.firebaseapp.com",
        databaseURL: "https://chattest-2f364.firebaseio.com",
        projectId: "chattest-2f364",
        storageBucket: "chattest-2f364.appspot.com",
        messagingSenderId: "923060517739"
    };




    constructor(public nav:NavController,
      public auth: AuthProvider,
      public userProvider: UserProvider,
      public util: UtilProvider,
      public storage:Storage,
      public af:AngularFire,
      public  params:NavParams,
      public alertCtrl: AlertController

    ) {


        this.user = params.data.user;



    }

    ngOnInit() {

    }
    ionViewWillEnter(){
      console.log("ENTER ionViewWillEnter");

      console.log("USER TO UPDATE");
      console.log(this.user)

      if (this.user.type ==  2001){
        this.kindUser = "admin"
      }else{
        this.kindUser = "normal"
      }

      this.nickname = this.user.nickname

    }

    saveUser(){
      let type = 0
      if (this.kindUser == "admin"){
        type = 2001
      }else{
        type = 2002
      }

      this.userProvider.updateUser(this.user.$key,type,this.nickname)
      this.nav.pop();
    }




    updatePassword(){
      this.resetPassword(this.user.email)
      const alert = this.alertCtrl.create({
        title: 'Aviso',
        message: 'Se enviara un email al correo del usuario para que pueda realizar el reseteo de su password',
        buttons: [
          {
            text: 'Aceptar',
            handler: () => {
              this.nav.pop();
            }
          }
        ]
      });
      alert.present();




    }
    resetPassword(email: string){
      return firebase.auth().sendPasswordResetEmail(email);
    }

  }

