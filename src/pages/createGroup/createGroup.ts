import { ChatsProvider } from './../../providers/chats-provider/chats-provider';
import { Component } from '@angular/core';
import { NavController, AlertController, App } from 'ionic-angular';
import { FirebaseListObservable } from 'angularfire2';
import { UserProvider } from '../../providers/user-provider/user-provider';
import { ChatViewPage } from '../chat-view/chat-view';
import { LoginPage } from '../login/login';

@Component({
    templateUrl: 'createGroup.html'
})
export class CreateGroupPage  {
    users:FirebaseListObservable<any[]>;
    listaChecks  = {};
    uid:string;
    nameGroup:String = "";



    constructor(
      public nav: NavController,
      public userProvider: UserProvider,
      public chatProvider: ChatsProvider,
      public alertCtrl: AlertController
    )
    {
      this.uid = userProvider.uid;
    }

    ngOnInit() {
      //Se obtienen todos los usuarios
      this.users = this.userProvider.getAllUsers();
      //Por cada usuario , creamos un diccionario donde tods los usuarios
      //tienen el atributo de false, para que cuando se haga click sobre el
      //el atribuo cambie a true y se indica si se agrega/quita del grupo
      this.users.forEach(element => {
          element.forEach(e=>{
              if(this.uid!=e.$key){
                  this.listaChecks[e.$key] = false;
              }else{
                  this.listaChecks[e.$key] = true;
              }
          });
      });
    };


    agregarRemoverUsuario(e:any){
        //De entrada todos los usuarios son "False", sin embargo cuando
        //se seleccionan , su estatus cambia a True
        this.listaChecks[e]=!this.listaChecks[e];
    }

    createGroup(){

        //Se valida que el nombre no exista (grupo)
        if(String(this.nameGroup)!=""){
          let existe = false
          let s = this.chatProvider.getAllGroups().subscribe(groups => {
            console.log("Inside For")
            console.log(groups);
            groups.forEach(group => {
              if(String(group.name).toLowerCase() == String(this.nameGroup).toLowerCase()){
                existe = true;

              }
            });
            if(!existe){
              var userChat  = {};
              for (var k in this.listaChecks){
                  var value = this.listaChecks[k];
                  if(k == this.uid){
                    userChat[k]=true;
                  }else{
                    if (value == true){
                      userChat[k]=false;
                    }
                  }
              }
              this.chatProvider.createGroup(this.nameGroup,userChat)
              this.nav.pop();
            }else{
              const alert = this.alertCtrl.create({
                title: 'Advertencia',
                message: 'Existe un grupo con el mismo nombre',
                buttons: [
                  {
                    text: 'Aceptar',
                  }
                ]
              });
              alert.present();

            }
            s.unsubscribe();
            console.log("Saliendo del Metodo")
          })
        }else{
          const alert = this.alertCtrl.create({
            title: 'Advertencia',
            message: 'El nombre del grupo no puede ir vacío',
            buttons: [
              {
                text: 'Aceptar',
              }
            ]
          });
          alert.present();

        }
          //Se recorren todos los usuarios para identificar cuales fueron
          //Seleccionados para el grupo , y se ponen en False, y solo el
          //Owner se pone en True , para que al vaciar la info el Owner se
          //Identifique como True

    }



}
