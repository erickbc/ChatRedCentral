import { CreateIncidentePage } from './../createIncidente/createIncidente';
import { InsidentePage } from './../incidentePage/incidentePage';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { UserProvider } from '../../providers/user-provider/user-provider';
import { ChatsProvider } from '../../providers/chats-provider/chats-provider';
import { AngularFire } from 'angularfire2';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { ChatViewPage } from '../chat-view/chat-view';
import { CreateGroupPage } from "../createGroup/createGroup";
import { ChatGroupViewPage } from "../chatGroup-view/chatGroup-view";
import { SearchPage } from "../search/search";
import { EditGroupPage } from "../editGroup/editGroup";

@Component({
    templateUrl: 'groups.html'
})
export class GroupsPage {
    groups:Observable<any[]>;
    //groupsParticipate:Observable<any[]>;
    viewIssuesPage = InsidentePage
    createGroupPage = CreateGroupPage;
    searchPage = SearchPage;
    uid:string;
    kindUser = 2001
    constructor(
        public chatsProvider: ChatsProvider,
        public userProvider: UserProvider,
        public af:AngularFire,
        public nav: NavController
    ) {
      this.kindUser = this.userProvider.kindUser
      //Se obtienen los grupos sobre los cuales esta incrito el usuario

        this.groups = this.chatsProvider.getGroups().map(groups => {
            return groups.map(group => {
                group.metadata = this.af.database.object(`/groups/${group.$key}`)
                //Revisar si es administrador del grupo
                return group;
            });
        });

      }






 openGroup(group) {
        //Extraemos los datos del grupo
        var ob:Observable<any> = group.metadata
          ob.subscribe(a=>{
              group = a
          });
        //==============================
        //Extraemos los participantes de la conversacion
        let interlocuters = this.af.database.list(`/groups/${group.$key}/users`).subscribe(
            keys => {
                let param = { group : group, users:keys,susb:interlocuters};
                this.nav.push(InsidentePage,param);
            }
        )
    }


    eliminar(group) {
        console.log("groups.ts - eliminar")
        console.log(group)
        //Extraemos los datos del grupo
        this.chatsProvider.deleteGroup(group.$key);
    }

     editar(group) {
        //Extraemos los datos del grupo
        var ob:Observable<any> = group.metadata
          ob.subscribe(a=>{
              group = a
          });
        //==============================
        //Extraemos los participantes de la conversacion
        let param = { group : group};
        this.nav.push(EditGroupPage,param);
    }



}
