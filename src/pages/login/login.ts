import { Component } from '@angular/core';
import { NavController, Events, MenuController, LoadingController, Loading } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TabsPage } from '../tabs/tabs';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { validateEmail } from '../../validators/email';
import { AuthProvider } from '../../providers/auth-provider/auth-provider';
import { UserProvider } from '../../providers/user-provider/user-provider';
import { UtilProvider } from '../../providers/utils';

@Component({
	templateUrl: 'login.html'
})
export class LoginPage {
    loginForm:any;
    token:any;
    loading: Loading;

    constructor(public nav:NavController,
      public auth: AuthProvider,
      public userProvider: UserProvider,
      public util: UtilProvider,
      public storage:Storage,

      public events: Events,
      private menu: MenuController,
      public loadingCtrl: LoadingController
    ) {



    }


    onLoading() {
        console.log("cargando - ViewMonitoreo");
    
        this.loading = this.loadingCtrl.create({
          content: 'Espere Porfavor...',
          dismissOnPageChange: false
        });
        this.loading.present();
    }
    onDismiss(){
        this.loading.dismiss();
    }
    

    ionViewDidEnter() {
        this.menu.swipeEnable(false);
    
        // If you have more than one side menu, use the id like below
        // this.menu.swipeEnable(false, 'menu1');
      }
    
      ionViewWillLeave() {
        // Don't forget to return the swipe to normal, otherwise 
        // the rest of the pages won't be able to swipe to open menu
        this.menu.swipeEnable(true);
    
        // If you have more than one side menu, use the id like below
        // this.menu.swipeEnable(true, 'menu1');
       }
       

    ngOnInit() {
       //Se crea el Login Form , con sus respectivos validadores
        this.loginForm = new FormGroup({
            email: new FormControl("",[Validators.required, validateEmail]),
            password: new FormControl("",Validators.required)
        });
    }


    resetLogin(){
      this.loginForm = new FormGroup({
        email: new FormControl("",[Validators.required, validateEmail]),
        password: new FormControl("",Validators.required)
    });
    }
    signin() {
        //Se autentifica el usuario
        this.onLoading();
        this.auth.signin(this.loginForm.value)
        .then((data) => {
            this.onDismiss();
            this.userProvider.uid =  data.uid;
            this.userProvider.token =  this.token;



            let susb = this.userProvider.getUserInfo(data.uid).subscribe(user =>{

              if(user.status){
                this.userProvider.kindUser =  user.type;
                this.userProvider.email = user.email;
                this.userProvider.nickname = user.nickname;
                this.userProvider.picture = user.picture;
                susb.unsubscribe();

                this.events.publish('username:kindUser',this.userProvider.kindUser);

                this.resetLogin()
                this.nav.setRoot(TabsPage);
              }else{
                susb.unsubscribe();
                this.auth.logout();

                let alert = this.util.doAlert("Error","Usuario Desabilitado","Ok");
                alert.present();

              }

            });

        }, (error) => {
            this.onDismiss();
        //Control de errores
            if (error["code"] == "auth/wrong-password"){
              let alert = this.util.doAlert("Error","Usuario o contraseñas incorrectas","Ok");
              alert.present();
            }else if(error["code"] == "auth/user-not-found"){
              let alert = this.util.doAlert("Error","Usuario no existe","Ok");
              alert.present();
            }
            else{
              let alert = this.util.doAlert("Error","Lo sentimos, intentalo mas tarde","Ok");
              alert.present();
            }
        });
    };

    createAccount() {
        let credentials = this.loginForm.value;
        this.auth.createAccount(credentials)
        .then((data) => {
          this.userProvider.uid =  data.uid;
          this.userProvider.email =  credentials.email;
          this.userProvider.token =  this.token;

          this.userProvider.createUser();
        }, (error) => {
            let alert;
            if (error["code"] == "auth/email-already-in-use"){
                alert = this.util.doAlert("Error","Usuario ya registrado","Ok");
            }else if (error["code"] == "auth/weak-password"){
                alert = this.util.doAlert("Error","Contraseña demasiado corta","Ok");
            }else{
                alert = this.util.doAlert("Error","Lo sentimos, intentalo mas tarde","Ok");
            }
            alert.present();
        });
    };
}
