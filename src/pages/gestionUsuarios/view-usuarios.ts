import { ViewEditUsuario } from './../editUser/view-editUser';
import { ViewCreateUsuarios } from './../createUser/view-createUser';

import { Component } from '@angular/core';
import { NavController, AlertController, Loading, LoadingController, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { UserProvider } from '../../providers/user-provider/user-provider';

@Component({
  selector: 'page-home',
  templateUrl: 'view-usuarios.html'
})
export class ViewUsuarios {

  //Usuarios
  users : any;
  uid :any
  loading: Loading;
  nombreUsuario: string = '';

  listaUsuarios= []
  listaUsuariosCopia= []
  idUserLogin = ""

  //Subscription Users
  subscription:any;
  constructor(private alertCtrl: AlertController,
              public navCtrl: NavController,
              private loadingCtrl: LoadingController,
              public userProvider: UserProvider

            ) {

              this.uid = this.userProvider.uid;
  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewAvisoLegalPage');
   //this.listaUsuarios = []
  }



  public loadUsuarios() {
    this.cargando()

    console.log("loadUsuarios()");
    //this.cargando()

    this.subscription = this.userProvider.getAllUsers().subscribe(users => {
      console.log("USUARIOS INSIDE");
      console.log(users);
      this.listaUsuarios = []
      for (const key in users) {
        this.listaUsuarios.push( users[key]);
      }
      this.listaUsuariosCopia = this.listaUsuarios
      this.loading.dismiss();
    });



  }



mostrarError(text) {
  this.loading.dismiss();

  let alert = this.alertCtrl.create({
    title: 'Error',
    subTitle: text,
    buttons: ['OK']
  });
  alert.present()
}


  cargando() {
    this.loading = this.loadingCtrl.create({
      content: 'Espere Porfavor...',
      dismissOnPageChange: false
    });
    this.loading.present();
  }

  busquedaUsuarios() {
    var nombre = this.nombreUsuario;
    if (!nombre) {
      this.listaUsuarios = this.listaUsuariosCopia;
      return;
    }
    this.listaUsuarios = this.listaUsuariosCopia.filter(x => x.email.indexOf(nombre) !== -1)
  }


  editarUsuario(user){

    console.log("====Editar Usuario====")
    console.log(user)
    let param = {user:user}
    this.navCtrl.push(ViewEditUsuario,param);
  }



  creaUsuario(){
    this.navCtrl.push(ViewCreateUsuarios);
  }

  retorno(){
    //this.navCtrl.setRoot(ViewIncidente);
  }

  eliminarUsuario(user){
    console.log("====Editar Usuario====")
    console.log(user)
    this.userProvider.disableUser(user.$key);


  }
  ionViewDidEnter(){
    console.log("====ionViewDidEnter====")

    this.loadUsuarios()
  }
  ionViewWillUnload(){
    console.log("====ionViewWillUnload====")
    this.subscription.unsubscribe();
  }

  ionViewWillLeave(){
    console.log("====ionViewWillLeave====")
    this.subscription.unsubscribe();
  }

}
