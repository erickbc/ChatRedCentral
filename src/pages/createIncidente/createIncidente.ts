import { ChatsProvider } from './../../providers/chats-provider/chats-provider';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { FirebaseListObservable } from 'angularfire2';
import { UserProvider } from '../../providers/user-provider/user-provider';
import { ChatViewPage } from '../chat-view/chat-view';

@Component({
    templateUrl: 'createIncidente.html'
})
export class CreateIncidentePage  {
    users:FirebaseListObservable<any[]>;
    //Usuarios para rectificar quien esta en el issue
    listaChecks  = {};
    //Only Keys de los Usuarios
    usersInGroup:any;
    //Infromation Users
    usersToIssue = []

    //Nombre del Issue
    nameIssue:String = "";
    //Grupo Perteneciente
    group:any;
    //uid del Usuario Actual
    uid:String = "";



    constructor(public nav: NavController, public userProvider: UserProvider,
        public chatProvider: ChatsProvider,
        public alertCtrl: AlertController,
        public  params:NavParams){
          //Informacion del Usuario
          this.uid = userProvider.uid
          //Informacion del Grupo
          this.group = params.data.group;
          this.usersInGroup = params.data.usersGroup;
    }

    ngOnInit() {
      /*
      //Se recorre la lista de usuarios y por cada usuario se extrae su informacion
      this.usersInGroup.forEach(element => {
        var u = this.userProvider.getUserInfo(element.$key).subscribe(user =>{
            this.usersToIssue.push(user)
            if(this.userProvider.uid!=element.$key){
              this.listaChecks[element.$key] = false;
            }else{
                this.listaChecks[element.$key] = true;
            }

        });
      });
      */
    };

    /*
    agregarQuitarUsuario(e:any){
        //De entrada todos los usuarios son "False", sin embargo cuando
        //se seleccionan , su estatus cambia a True
        this.listaChecks[e]=!this.listaChecks[e];
    }

    */

    createIssue(){
      //Se recorre la lista para descartar las persoas que no se
      // deasean agregar al issue
      if(String(this.nameIssue)!=""){
        let existe = false
        let s = this.chatProvider.getAllIssues().subscribe(issues => {

          issues.forEach(issue => {
            if(issue.idGroup == this.group.$key){
              if(String(issue.name).toLowerCase() == String(this.nameIssue).toLowerCase()){
                existe = true;
              }
            }
          });
          if(!existe){
            var userChat  = {};
            /*
            for (var k in this.listaChecks){
                var value = this.listaChecks[k];
                if(k == this.userProvider.uid){
                  userChat[k]=true;
                }else{
                  if (value == true){
                    userChat[k]=false;
                  }
                }
            }*/
            this.chatProvider.createIssue(this.nameIssue, this.group)
            this.nav.pop();
          }else{
            const alert = this.alertCtrl.create({
              title: 'Advertencia',
              message: 'Existe un incidente con el mismo nombre',
              buttons: [
                {
                  text: 'Aceptar',
                }
              ]
            });
            alert.present();

          }
          s.unsubscribe();
          });
        }else{
            const alert = this.alertCtrl.create({
              title: 'Advertencia',
              message: 'El nombre de la incidencia no puede ir vacía',
              buttons: [
                {
                  text: 'Aceptar',
                }
              ]
            });
            alert.present();

          }




    }



}
