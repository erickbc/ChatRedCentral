import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { AuthProvider } from '../../providers/auth-provider/auth-provider';
import { UserProvider } from '../../providers/user-provider/user-provider';

@Component({
    templateUrl: 'account.html'
})
export class AccountPage {
    rootNav;
    user = {};
    constructor(public nav: NavController,
                public auth: AuthProvider,
                public userProvider: UserProvider,
                public local:Storage) {


                this.user = {picture:this.userProvider.picture}
    }



    sendPicture() {
      this.userProvider.getPicture()
      .then((image) => {
          this.userProvider.updatePicture(image)
          this.user = {picture:image}
      });
    }


    logout() {
        this.local.remove('uid');
        this.auth.logout();
    }
}
