import { InsidentePage } from './../incidentePage/incidentePage';
import { LastChatPage } from './../lastChats/lastChat';
import { GroupsPage } from './../groups/groups';
import { Component } from '@angular/core';
import { ChatsPage } from '../chats/chats';
import { AccountPage } from '../account/account';
import { UsersPage } from '../users/users';

@Component({
	selector: 'tabs-page',
	templateUrl: 'tabs.html'
})
export class TabsPage {
  //chats = ChatsPage;
  //Ultimos Chats
  //users = LastChatPage;
  //Ultimos Configuracion Cuenta / Proximamente Logout
  profile = AccountPage;
  //Grupos
	groups = GroupsPage;
}
