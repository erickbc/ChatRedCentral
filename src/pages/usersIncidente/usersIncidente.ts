import { ChatsProvider } from './../../providers/chats-provider/chats-provider';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FirebaseListObservable, AngularFire } from 'angularfire2';
import { UserProvider } from '../../providers/user-provider/user-provider';
import { ChatViewPage } from '../chat-view/chat-view';

@Component({
    templateUrl: 'usersIncidente.html'
})
export class UsersIncidentePage  {

    //Users with informattion que no estan en el issue
    usersNotInGroupDataObject  = [];
    //Users with informattion y que estan el el issue
    usersInGroupDataObject= [];
     //Users en el issue solo keys
    userInGroupKeys= [];
    //Users no el issue (solo keys)
    usersNotInGroupKeys= [];
    //Key del Usuario Logeado
    uid:string;
    //Key del Issue
    keyIssue = ""
    //Lita de Usuarios para agregar al grupo
    listachecks=[]
    //Informacion del grupo
    group:any;
    //Nombre del Issue
    nameIssue:String = "";
    //Tipo de Modificacion
    type:string;

    //Subcriptions
    sub1 : any;

    constructor(public nav: NavController,
        public userProvider: UserProvider,
        public chatProvider: ChatsProvider,
        public af:AngularFire,
        public  params:NavParams,){
            //ID DEL Usuario Logeado
            this.uid = userProvider.uid;
            //Obtiene el array de Uusarios
            //con Infrmacion para ser desplegada en pantalla
            //this.usersNotInGroupDataObject = params.data.usersNotInGroupDataObject;

            this.keyIssue = params.data.keyIssue;

            //Obtiene el array de Uusarios que se encuentran actualmente en el grupo
            //con Infrmacion para ser desplegada en pantalla
            //this.usersInGroupDataObject = params.data.usersInGroupDataObject;
            //Obtiene el array de keys deUusarios que se
            /// encuentran actualmente en el grupo

            this.userInGroupKeys = params.data.userInGroupKeys;
            //Obtiene el array de keys deUusarios que no se
            /// encuentran actualmente en el issue
            this.usersNotInGroupKeys = params.data.usersNotInGroupKeys;
            //Informacion del grupo
            this.group = params.data.group;
            //Nombre del Issue/Grupo
            this.nameIssue = params.data.nameIssue;
            //Tipo G=Grupo I=Issue
            this.type = params.data.type;

    }


    ngOnInit() {
      console.log("[editGroup.ts] - ngOnInit")
    //Se obtienen todos los usuarios
    //Por cada usuario , creamos un diccionario donde tods los usuarios
    //tienen el atributo de false, para que cuando se haga click sobre el
    //el atribuo cambie a true y se indica si se agrega/quita del grupo
    this.usersNotInGroupKeys.forEach(element => {
      this.sub1 = this.userProvider.getUserInfo(element).subscribe(usuario =>{
            this.usersNotInGroupDataObject.push(usuario)
        });
    });



    };
    //Metodo para determinar si un elemento esta en un array
    isInArray(value, array) {
        return array.indexOf(value) > -1;
    }


    //Agrega o quitar usarios por agregar al Issue
    agregarQuitarUsuario(e:any){
        if(!this.isInArray(e,this.listachecks)){
            this.listachecks.push(e)
        }else{
            var index = this.listachecks.indexOf(e)
            this.listachecks.splice(index,1)
        }
        console.log(this.listachecks);
    }

    //Agrega Uusarios al Incidente
    saveUsersIssue(){
        var list = {}
        let data = {
            dateUpdate: new Date().toISOString(),
            idGroup:  this.group.$key,
            lastMessage:  "",
            nameGroup:  this.group.name,
            nameIssue: this.nameIssue,
        }
        //Por cada usuario que se agrego a la lista ,
        //se le debe de enviar a su perfil el issue correspondientepara
        this.listachecks.forEach(usuario => {
            list[usuario] = false
            this.chatProvider.updateUsersIssueInformation(data,usuario,this.keyIssue)
        });
        //Se iteran los usuarios que ya existen ,
        //para hacer una lista "total" de usuarios
        this.userInGroupKeys.forEach(usuario => {
            if(usuario==this.uid){
                list[usuario] = true
            }else{
                list[usuario] = false
            }
        });
        this.chatProvider.updateUsersIssue(list,this.keyIssue)
        this.nav.pop();
    }

    ionViewWillUnload(){
      console.log("[editGroup.ts] - ionViewWillUnload")
      this.sub1.unsubscribe();
    }

    saveUsersGroup(){
        var list = {}

        let data = {
          dateUpdate: new Date().toISOString(),
          idGroup:  this.group.$key,
          lastMessage:  "",
          nameGroup:  this.group.name,
          nameIssue: this.nameIssue,
      }
        //Por cada usuario que se agrego a la lista ,
        //se le debe de enviar a su perfil el issue correspondientepara
        this.listachecks.forEach(usuario => {
            list[usuario] = false
            this.chatProvider.updateUsersIssueInformation(data,usuario,this.keyIssue)
        });
        //Se iteran los usuarios que ya existen ,
        //para hacer una lista "total" de usuarios
        this.userInGroupKeys.forEach(usuario => {
            if(usuario==this.uid){
                list[usuario] = true
            }else{
                list[usuario] = false
            }
        });
        this.chatProvider.updateUsersGroup(list,this.keyIssue);





        this.nav.pop();
    }

    saveUsers(){

        if(this.type == "G"){
            this.saveUsersGroup();
        }else{
            this.saveUsersIssue();
        }

    }



}
