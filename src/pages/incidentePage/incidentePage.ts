import { EditIncidentePage } from './../editIncidente/editIncidente';
import { ChatGroupViewPage } from './../chatGroup-view/chatGroup-view';
import { Observable } from 'rxjs/Observable';
import { CreateIncidentePage } from './../createIncidente/createIncidente';
import { ChatsProvider } from './../../providers/chats-provider/chats-provider';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { FirebaseListObservable, AngularFire } from 'angularfire2';
import { UserProvider } from '../../providers/user-provider/user-provider';

@Component({
    templateUrl: 'incidentePage.html'
})
export class InsidentePage  {
    //users:FirebaseListObservable<any[]>;
    listaChecks  = {};
    uid:string;
    nameUser:string;
    group:any;
    interlocutors:[string];
    //groupIssues:FirebaseListObservable<any[]>;
    groupIssues:Observable<any[]>;
    kindUser=2001
    constructor(
        public nav: NavController,
        public userProvider: UserProvider,
        public chatProvider: ChatsProvider,
        public  params:NavParams,
        public af:AngularFire,
        public chatsProvider: ChatsProvider,
        public alertCtrl: AlertController
     ) {
        this.kindUser = this.userProvider.kindUser
        //Datos del Usuario (Login)
        this.uid = userProvider.uid
        this.nameUser = userProvider.email
        //Datos del Datos del Grupo
        this.interlocutors = params.data.users;
        this.group = params.data.group;

        params.data.susb.unsubscribe();
    }

    ngOnInit() {
        //Se consultan los issues de acuerdo al grupo seleccionado
        this.groupIssues = this.chatsProvider.getIssuesInGroup(this.group.$key).map(groups => {
          return groups.map(issue => {
              issue.metadata = this.af.database.object(`/issues/${issue.$key}`)
              return issue;
          });
        });
    };

    //Abre Pagina para Crear Issue
    createIssuePage(){
        let param = {group : this.group,usersGroup:this.interlocutors};
        this.nav.push(CreateIncidentePage,param);
    }
    //Abre la venta del chat
    openChat(key) {
      //Extraemos la informacion del issue
      this.af.database.object(`/issues/${key}/name`).
        subscribe(snapshot => {
          var nameIssue = snapshot.$value
          //Key del Grupo, Nombre del Grupo y Nombre del Issue
          let param = {keyGroup : key,nameGroup:this.group.name,nameIssue:nameIssue,idGroup:this.group.$key};
          this.nav.push(ChatGroupViewPage,param);
        }).unsubscribe();
    }

    //Edita Incidente Nombre y Usuarios
    editIncidente(key){
        console.log("[IncidentePage.ts] - editIncidente")

        let param = {group : this.group,keyIssue:key};
        console.log("======Params Incidente======")
        console.log(param)
        this.nav.push(EditIncidentePage,param);

    }
    //Deshabilita el incidente
    turnOff(key){
        const alert = this.alertCtrl.create({
            title: 'Borrar Issue',
            message: '¿En realidad quieres borrar el siguiente issue?',
            buttons: [
              {
                text: 'Cancelar',
                role: 'cancel',

              },
              {
                text: 'Aceptar',
                handler: () => {
                  this.chatProvider.deleteIssue(this.group.$key,key)
                }
              }
            ]
          });
          alert.present();
    }

}
