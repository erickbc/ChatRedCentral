import { Observable } from 'rxjs/Observable';
import { UsersIncidentePage } from './../usersIncidente/usersIncidente';
import { ChatsProvider } from './../../providers/chats-provider/chats-provider';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FirebaseListObservable, AngularFire } from 'angularfire2';
import { UserProvider } from '../../providers/user-provider/user-provider';
import { ChatViewPage } from '../chat-view/chat-view';

@Component({
    templateUrl: 'editIncidente.html'
})
export class EditIncidentePage  {



    //Lista de Usuarios Pertencientes al Issue (DataObjects)
    usersToIssue = []
    //Nombre del Issue
    nameIssue:String = "";
    //Solo Key de los Usuarios para la comparacion al agregar usuarios
    usersKeys  = [];
    //Informacion del grupo
    group:any;
    //Uid Personal
    uid:string;
    //Key del Grupo
    keyIssue:any;

    constructor(public nav: NavController,
        public userProvider: UserProvider,
        public chatProvider: ChatsProvider,
        public af:AngularFire,
        public  params:NavParams){

        this.uid = userProvider.uid;
        this.group = params.data.group;
        this.keyIssue = params.data.keyIssue;

    }
    //Carga informacion de la pantalla usuarios / nombre del Issue

    ionViewWillEnter() {
        console.log("[editIcidente.ts] - ionViewWillEnter")
        this.chatProvider.getAllUsersIssue(this.keyIssue).
            subscribe(snapshot => {
                //Reiniciamos valor de la lista
                //this.usersKeys = []
                //Reiniciamos valor de la lista
                //this.usersToIssue=[]
                //Extraemos el nombre del Issue
                this.nameIssue = snapshot.name
                //Extraemos la informacion de cada usuario
                /*
                for(var u in snapshot.users){
                    this.userProvider.getUserInfo(u).subscribe(user =>{
                        //Extraemos solamente el key
                        this.usersKeys.push(user.$key)
                        //Extraemos el usuario completo
                        this.usersToIssue.push(user)

                    });
                }
                */
        });
    };
    /*
    //Function to get difference betwwen two arrays
    arr_diff (a1, a2) {
        console.log("[editIcidente.ts] - arr_diff")
        var a = [], diff = [];

        for (var i = 0; i < a1.length; i++) {
            a[a1[i]] = true;
        }

        for (var i = 0; i < a2.length; i++) {
            if (a[a2[i]]) {
                delete a[a2[i]];
            } else {
                a[a2[i]] = true;
            }
        }

        for (var k in a) {
            diff.push(k);
        }

        return diff;
    }
    */

    /*
    //Se agregan usuarios al Issue
    addUsers(){
        console.log("[editIcidente.ts] - addUsers")
        let usuariosInGroup = []

        for (var key in this.group.users) {
            usuariosInGroup.push(key)
        }
        //Obtenemos la Diferiencia de Usuarios
        let difference = this.arr_diff(this.usersKeys,usuariosInGroup);

        var userWithInformation = []
        //De la diferencia de Usuarios obtenemos la informacion de cada uno
        difference.forEach(u => {
            this.userProvider.getUserInfo(u).subscribe(user =>{
                userWithInformation.push(user)
            });
        });
        //Con la informacion obtenida ,
        //mandamos a  traer a la pantalla para la seleccion de nuevos usuarios
        let param = {
                    usersInGroupDataObject:this.usersToIssue,
                    userInGroupKeys:this.usersKeys,
                    usersNotInGroupKeys: difference,
                    usersNotInGroupDataObject:userWithInformation,
                    keyIssue:this.keyIssue,
                    group:this.group,
                    nameIssue : this.nameIssue,
                    kind:"I"
        };

        this.nav.push(UsersIncidentePage,param);
    }*/
    /*
    //Se borran usuarios del issue "!!Solo del Issue!!!!"
    deleteUser(key){
        console.log("[editIcidente.ts] - deleteUser")

        for (var u in this.usersToIssue){
            if(this.usersToIssue[u].$key==key){
                this.usersToIssue.splice(parseInt(u), 1);
                delete this.usersKeys[key]
            }
        }


        var users = this.af.database.list(`/issues/${this.keyIssue}/users`);
            users.remove(key).then(_ => console.log('item deleted!'));

    }
    */


    saveIssue(){
        this.chatProvider.updateIssue(this.nameIssue, this.keyIssue);
        this.nav.pop();
    }






}
