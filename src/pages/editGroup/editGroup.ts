import { Observable } from 'rxjs/Observable';
import { UsersIncidentePage } from './../usersIncidente/usersIncidente';
import { ChatsProvider } from './../../providers/chats-provider/chats-provider';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { FirebaseListObservable, AngularFire } from 'angularfire2';
import { UserProvider } from '../../providers/user-provider/user-provider';
import { ChatViewPage } from '../chat-view/chat-view';

@Component({
    templateUrl: 'editGroup.html'
})
export class EditGroupPage  {


    usuariosParaMostrar:any;
    //Lista de Usuarios Pertencientes al Grupo (DataObjects)
    usersToGroup = []
    usersToGroupKey = {}
    //Nombre del Issue
    nameGroup:String = "";
    //Solo Key de los Usuarios para la comparacion al agregar usuarios
    usersKeys  = [];
    //Informacion del grupo
    group:any;
    //Uid Personal
    uid:string;
    //Key del Grupo
    keyGroup:any;
    //Total Usuarios
    usuariosTotal = []

    kindUser = 2001

    //Subcriptions
    sub1 : any;
    sub2 : any;
    constructor(public nav: NavController, public userProvider: UserProvider,
        public chatProvider: ChatsProvider,
        public af:AngularFire,
        public alertCtrl: AlertController,
        public  params:NavParams){

        this.uid = userProvider.uid;
        this.group = params.data.group;

        this.nameGroup = params.data.group.name


        this.kindUser = this.userProvider.kindUser





    }
    //Carga informacion de la pantalla usuarios / nombre del Issue

    ngOnInit() {
        console.log("[editGroup.ts] - ngOnInit")
      //Se obtienen todos los usuarios
      //Por cada usuario , creamos un diccionario donde tods los usuarios
      //tienen el atributo de false, para que cuando se haga click sobre el
      //el atribuo cambie a true y se indica si se agrega/quita del grupo



    };

    ionViewWillEnter() {
        console.log("[editGroup.ts] - ionViewWillEnter")

        this.chatProvider.getAllUsers().forEach(element => {
          element.forEach(e=>{
              if (e.status){
                this.usuariosTotal.push(e.$key);
              }

          });
      });

      this.usuariosParaMostrar = this.chatProvider.getListUsersGroup(this.group.$key).map(usuarios => {
        console.log("USUARIOS")
        console.log(usuarios)
        return usuarios.map(usuario => {
            usuario.metadata = this.userProvider.getUserInfo(usuario.$key)
            //this.af.database.object(`/groups/${group.$key}`)
            //Revisar si es administrador del grupo
            return usuario;
        });
    });

    };
    removeDuplicateUsingFilter(arr){
        console.log("[editGroup.ts] - removeDuplicateUsingFilter")
        let unique_array = arr.filter(function(elem, index, self) {
            return index == self.indexOf(elem);
        });
        return unique_array
    }

    keys() : Array<string> {
      return Object.keys(this.usersToGroup);
    }

    //Function to get difference betwwen two arrays
    arr_diff (a1, a2) {
        console.log("[editGroup.ts] - arr_diff")
        var a = [], diff = [];
        for (var i = 0; i < a1.length; i++) {
            a[a1[i]] = true;
        }
        for (var i = 0; i < a2.length; i++) {
            if (a[a2[i]]) {delete a[a2[i]];}
            else{a[a2[i]] = true;}
        }
        for (var k in a) {diff.push(k);}
        return diff;
    }



    //Se agregan usuarios al Issue
    addUsers(){
        console.log("[editGroup.ts] - addUsers")
        console.log(this.usuariosParaMostrar)
        console.log("========================================")
        let usuariosInTheApp = this.removeDuplicateUsingFilter(this.usuariosTotal);
        let usuariosInTheGroup = []
        let s : any;
        s = this.usuariosParaMostrar.subscribe(e =>{
          e.forEach(element => {
            console.log("ELEMENTOS")
            console.log(element)
            usuariosInTheGroup.push(element.$key)
          });
          let difference = this.arr_diff(usuariosInTheApp,usuariosInTheGroup);

          let param = {
            //usersInGroupDataObject:this.usersToGroup,
            userInGroupKeys:usuariosInTheGroup,
            usersNotInGroupKeys: difference,
            //usersNotInGroupDataObject:userWithInformation,
            keyIssue:this.group.$key,
            group:this.group,
            nameIssue : this.nameGroup,
            type:"G"
          };
          s.unsubscribe();
          this.nav.push(UsersIncidentePage,param);

        });




         /*
        //Obtenemos la Diferiencia de Usuarios

        var userWithInformation = []
        //De la diferencia de Usuarios obtenemos la informacion de cada uno
        difference.forEach(u => {
            this.userProvider.getUserInfo(u).subscribe(user =>{
                userWithInformation.push(user)
            });
        });
        */
        //Con la informacion obtenida ,
        //mandamos a  traer a la pantalla para la seleccion de nuevos usuarios

    }

    //Se borran usuarios del issue "!!Solo del Issue!!!!"
    deleteUser(key){

        console.log("[editGroup.ts] - deleteUser")
        delete this.usersToGroupKey[key]




        //Lo elimina de la lista que se ve en pantalla
        for (var u in this.usersToGroup){
            if(this.usersToGroup[u].$key==key){
                this.usersToGroup.splice(parseInt(u), 1);
                delete this.usersKeys[key]
            }
        }
        //Lo elimina de su perfil
        this.chatProvider.deleteUserGroup(key,this.group.$key);




        //var users = this.af.database.list(`/issues/${this.keyIssue}/users`);
         //   users.remove(key).then(_ => console.log('item deleted!'));

    }

    admin(keyUser){
      this.sub1.unsubscribe();
      this.sub2.unsubscribe();

      var  alert = this.alertCtrl.create({
        title: 'Aviso',
        message: '¿Deseas establecer como administrador a este usuario?',
        buttons: [

          {
            text: 'Aceptar',
            handler: () => {
              this.chatProvider.setAdminGroup(keyUser,this.group.$key);
            }
          },
          {
            text: 'Cancelar',
          }
        ]
      });

      alert.present()

    }
    saveGroup(){
      this.chatProvider.saveGroup(this.nameGroup, this.group.$key);
      this.nav.pop();
    }






}
