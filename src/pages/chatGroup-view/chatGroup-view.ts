import { PushNotificationProvider } from './../../providers/rest-provider/pushNotification-provider';
import { Observable } from 'rxjs/Observable';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Content, AlertController, App } from 'ionic-angular';
import { AngularFire, FirebaseListObservable } from 'angularfire2';
import { ChatsProvider } from '../../providers/chats-provider/chats-provider';
import { UserProvider } from '../../providers/user-provider/user-provider';
import 'rxjs/add/operator/do';
import { LoginPage } from '../login/login';

@Component({
  templateUrl: 'chatGroup-view.html',
})
export class ChatGroupViewPage {
  //Mensaje a enviar
  message: string;


  //Datos del Usuario
  uid:string;
  nameUser:string;

  //interlocutors:[string];

  //Mensajes del Chat
  chats:FirebaseListObservable<any>;
  //Subcriptor Grupo
  subscriptorGrupo:any
  //Subcriptor User
  subscriptorUser:any
  //Datos del Grupo
  keyGroup:string;
  nameGroup:string;
  idGroup:string;
  nameIssue:string;
  nickname:string;
  prom:any;
  sub:any;
  @ViewChild(Content) content: Content;
  constructor(
      public nav:NavController,
      private params:NavParams,
      public chatsProvider:ChatsProvider,
      public af:AngularFire,
      public userProvider:UserProvider,
      public pushProvider:PushNotificationProvider,
      public alertCtrl: AlertController,
      public app: App,

  ) {
      //Datos Personales del Usuario
      this.uid = userProvider.uid;
      this.nameUser = userProvider.email;
      this.nickname = userProvider.nickname

      //Datos del Grupo y del Issue
      this.keyGroup = params.data.keyGroup;
      this.nameGroup = params.data.nameGroup;
      this.nameIssue = params.data.nameIssue;
      this.idGroup = params.data.idGroup;

      this.checkObserverGroup()
      this.checkObserverUser()
      //Debe de extraer todos los mensajes del chat
      this.loadChat(this.keyGroup)
        .then((chatRef:any) => {
            this.chats = this.af.database.list(chatRef);
      });


  }

  checkObserverGroup(){
    this.subscriptorGrupo = this.chatsProvider.getIssue(this.keyGroup).subscribe(i =>{
      if(!i.status){
        try {
          this.subscriptorGrupo.unsubscribe();
          this.subscriptorUser.unsubscribe();
        } catch (error) {
          console.log("=============ERROR TO UNSUBCRIBE GROUP====================");
          console.log(error);


        }

        let alert = this.alertCtrl.create({
          title: 'Aviso',
          message: 'EL GRUPO O INCIDENTE HA SIDO DADO DE BAJA,CONSULTALO CON TU ADMINISTRADOR',
          buttons: [
            {
              text: 'Aceptar',
              handler: () => {

                this.nav.pop();
              }
            }
          ]
        });
        alert.present()
      }
    });
  }

  checkObserverUser(){
    this.subscriptorUser = this.userProvider.getUser(this.uid).subscribe(u =>{
      console.log("=============GETTING USSER STATUS====================");
      console.log(u);
      if(!u.status){
        try {
          this.subscriptorGrupo.unsubscribe();
          this.subscriptorUser.unsubscribe();
        } catch (error) {
          console.log("=============ERROR TO UNSUBCRIBE USER====================");
          console.log(error);


        }
        let alert = this.alertCtrl.create({
          title: 'Aviso',
          message: 'HAS SIDO DADO DE BAJA,CONSULTALO CON TU ADMINISTRADOR',
          buttons: [
            {
              text: 'Aceptar',
              handler: () => {

                this.app.getRootNav().setRoot(LoginPage);
              }
            }
          ]
        });
        alert.present()
      }
    });
  }

  loadChat(key){
    let firstRef = this.af.database.object(`/chats/${key}`, {preserveSnapshot:true});
    let promise = new Promise((resolve, reject) => {
        this.sub=firstRef.subscribe(snapshot => {
            //let a = snapshot.exists();
            console.log(snapshot)
            resolve(`/chats/${key}`);
            if(this.content){
                this.content.scrollToBottom();
            }
        });
    });
  return promise;
  }


  ionViewDidEnter() {
      this.content.scrollToBottom();
  }


  ionViewDidLoad(){
    this.chatsProvider.content = this.content
  }

  sendMessage() {
    if(this.message) {
      //Estructura del Mensaje
      let chat = {
          from: this.uid,
          nameFrom:this.nickname,
          message: this.message,
          type: 'message'
          //picture:this.userProvider.p
      };
      console.log("Extrutura del Mensaje");
      console.log(this.keyGroup);

      //Se agrega el mensaje a la lista del chat, al ser promise se agrega automaticamente
      this.chats.push(chat).then((item) =>{
        //Guardamos el ultimo mensaje
        let grp = this.af.database.object(`/issues/${this.keyGroup}/lastMessage`);
        //Fecha del utlimo mensaje
        let dateUpdate = this.af.database.object(`/issues/${this.keyGroup}/dateUpdate`);
          dateUpdate.set(new Date().toISOString());
          grp.set(this.message);
        //Scrolleamos al bottomm

            this.content.scrollToBottom();

        console.log("=====ITEM KEY======");
        console.log(item.key);
        let interlocuters = this.af.database.list(`/groups/${this.idGroup}/users`);
          interlocuters.forEach(element => {
              element.forEach(e => {
                  //Extraemos el key del usuario e.$key
                  var userToken = this.af.database.object(`/users/${e.$key}/token`);
                  //Extraemos el Token del User y Mnadamos la Push Notification
                  userToken.subscribe(snapshot => {
                      var header = this.nameGroup + "-" + this.nameIssue
                      this.pushProvider.postRequest(header,chat.message,snapshot.$value)
                  })


                  let lastUpdateIssueUser = this.af.database.object(`/users/${e.$key}/issues/${this.keyGroup}/dateUpdate`);
                      lastUpdateIssueUser.set(new Date().toISOString());
                      console.log("DATA TO SAVE MESSAGE");
                      console.log("USER::"+e.$key);
                      console.log("KEY ISSUE:::"+this.keyGroup);
                      console.log(":::::MESSAGE:::"+chat.message);
                  let lastMessageUser = this.af.database.object(`/users/${e.$key}/issues/${this.keyGroup}/lastMessage`);
                      lastMessageUser.set(chat.message)

                  let lastMessageIssue = this.af.database.object(`/issues/${this.keyGroup}/lastMessage`);
                      lastMessageIssue.set(chat.message)


                  let isReadMessageUser = this.af.database.object(`/users/${e.$key}/issues/${this.keyGroup}/isRead`);
                      isReadMessageUser.set(false);

                      this.af.database.object(`/issues/${this.keyGroup}`)
              });
          });


        //------
      } );
          this.message = "";
      }
  };

  ionViewWillUnload(){
    console.log("ionViewWillUnload")

     let isReadMessageUser = this.af.database.object(`/users/${this.uid}/issues/${this.keyGroup}/isRead`);
        isReadMessageUser.set(true);

    try {
      this.subscriptorGrupo.unsubscribe();
      this.subscriptorUser.unsubscribe();
    } catch (error) {
      console.log("=============ERROR TO UNSUBCRIBE GROUP====================");
      console.log(error);


    }

    this.sub.unsubscribe();
    //this.nav.pop();
  }

  sendPicture() {
    let chat = {from: this.uid, type: 'picture', picture:null};
    this.userProvider.getPicture()
    .then((image) => {
        chat.picture =  image;
        this.chats.push(chat);
    });
  }

}
