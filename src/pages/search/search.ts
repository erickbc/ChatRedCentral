import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { UserProvider } from '../../providers/user-provider/user-provider';
import { ChatsProvider } from '../../providers/chats-provider/chats-provider';
import { AngularFire } from 'angularfire2';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { ChatViewPage }  from '../chat-view/chat-view';

import { ChatGroupViewPage } from "../chatGroup-view/chatGroup-view";

@Component({
    templateUrl: 'search.html'
})
export class SearchPage {
    chats:Observable<any[]>;
    searchTerm: string = '';
    items: any;
    nameUser:string;
    uid:string;
    issues = []
    listaGroupsMessages = []
    constructor(public chatsProvider: ChatsProvider,
        public userProvider: UserProvider,
        public af:AngularFire,
        public nav: NavController,

        ) {




        this.chatsProvider.getIssuesRolled().forEach(a => {
            a.forEach(element => {
              this.chatsProvider.getGroup(element.idGroup).subscribe(group =>{
                if(group.status){
                  this.issues.push(element)
                }
              })
            });

        })
        }


    ionViewDidLoad() {
        console.log("ionViewDidLoad")
        //this.getItems();
    }



  getItems(){
    console.log("Busqueda")
    var testChats
    var  q = this.searchTerm;
        // if the value is an empty string don't filter the items
        if (!q) {
            return;
        }
        this.listaGroupsMessages = []
        this.issues.forEach(issue => {
          var listaMessages = []
          var find = false
          this.chatsProvider.getChatMessages(issue.$key).forEach(m =>{
            m.forEach(mensaje => {
              if (mensaje.message.indexOf(q) !== -1){
                  find = true
                  listaMessages.push( {
                      keyMessage:mensaje.$key,
                      message:mensaje.message
                  })
              }
            });
            if (find){
              this.listaGroupsMessages.push({
                      keyIssue : issue.$key,
                      nameGroup:issue.nameGroup,
                      nameIssue:issue.nameIssue,
                      messages: listaMessages
              });
            }
          })
        });
    }


 openChat(issue) {
  console.log("lastChat.ts - openChat");
  console.log(issue);
  let param = {
    keyGroup:issue.keyIssue,
    nameGroup:issue.nameGroup,
    nameIssue:issue.nameIssue
  }
  this.nav.push(ChatGroupViewPage,param);

}


}
