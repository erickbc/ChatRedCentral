import { Component } from '@angular/core';
import { NavController, AlertController, Loading, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TabsPage } from '../tabs/tabs';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { validateEmail } from '../../validators/email';
import { AuthProvider } from '../../providers/auth-provider/auth-provider';
import { UserProvider } from '../../providers/user-provider/user-provider';
import { UtilProvider } from '../../providers/utils';
import { AngularFire, AngularFireModule, AuthProviders, AuthMethods,FirebaseAuth } from 'angularfire2';
import * as firebase from 'firebase';



@Component({
  selector: 'page-home',
  templateUrl: 'view-createUser.html'
})
export class ViewCreateUsuarios {
    loginForm:any;
    token:any;
    kindUser: any = 'normal';
    loading: Loading;


    constructor(public nav:NavController,
      public auth: AuthProvider,
      public userProvider: UserProvider,
      public util: UtilProvider,
      public storage:Storage,
      public af:AngularFire,
      public alertCtrl: AlertController,
      private loadingCtrl: LoadingController,

    ) {



    }


  cargando() {
      this.loading = this.loadingCtrl.create({
        content: 'Espere Porfavor...',
        dismissOnPageChange: true
      });
      this.loading.present();
    }

    ngOnInit() {
       //Se crea el Login Form , con sus respectivos validadores
        this.loginForm = new FormGroup({
            email: new FormControl("",[Validators.required, validateEmail]),
            password: new FormControl("",Validators.required),
            options: new FormControl("",Validators.required),
            nickname: new FormControl("",Validators.required)
        });
    }


    createAccount() {

        this.cargando();
        let credentials = this.loginForm.value;

        let firebaseConfig = {
          apiKey: "AIzaSyBEHjhEuZ3ovv16OkDa0pV1p_hm0l5UDAM",
          authDomain: "testfirebase-d2d5b.firebaseapp.com",
          databaseURL: "https://testfirebase-d2d5b.firebaseio.com",
          projectId: "testfirebase-d2d5b",
          storageBucket: "testfirebase-d2d5b.appspot.com",
          messagingSenderId: "373781110730"
        };






        //var secondaryApp = AngularFireModule.initializeApp(firebaseConfig, myFirebaseAuthConfig)
        var secondaryApp :any;

        try {
          secondaryApp = firebase.initializeApp(firebaseConfig, "Secondary");
        } catch (error) {
          console.log(error)
        }

        secondaryApp.auth().createUserWithEmailAndPassword(credentials.email,credentials.password ).then((data) => {
          //this.af.auth.createUser(credentials)

          secondaryApp.auth().signOut();
          this.userProvider.createUserEmail(data.uid,credentials.email,credentials.options,credentials.nickname);


        }).then(()=>{
          this.loading.dismiss();
          secondaryApp.delete();

          this.nav.pop();
        }).catch(error =>{
          console.log("Errro");
          console.log(error);
          let alert;
            if (error["code"] == "auth/email-already-in-use"){
                let status = true;
                let keyUser:any;
                let su = this.userProvider.getAllUsers().subscribe(users => {
                  users.forEach(user => {
                    if(user.email == credentials.email ){
                      status = user.status;
                      keyUser = user.$key;
                    }
                  });
                  su.unsubscribe();
                  this.loading.dismiss();
                  if(status){
                    //alert = this.util.doAlert("Error","Usuario ya registrado","Ok");
                    alert = this.alertCtrl.create({
                      title: 'Aviso',
                      message: 'Usuario ya registrado',
                      buttons: [

                        {
                          text: 'Aceptar',
                          handler: () => {
                            this.nav.pop();
                          }
                        }
                      ]
                    });

                  }else{
                    alert = this.alertCtrl.create({
                      title: 'Aviso',
                      message: 'Usuario dado de baja, ¿Deseas reactivarlo?',
                      buttons: [
                        {
                          text: 'NO',
                          handler: () => {
                            this.nav.pop();
                          }

                        },
                        {
                          text: 'SI',
                          handler: () => {
                            this.userProvider.activateUser(keyUser);
                            this.nav.pop();
                          }
                        }
                      ]
                    });
                  }
                  alert.present()



                })
                //

            }else if (error["code"] == "auth/weak-password"){
              this.loading.dismiss();
                alert = this.alertCtrl.create({
                  title: 'Aviso',
                  message: 'Contraseña demasiado corta',
                  buttons: [

                    {
                      text: 'Aceptar',
                    }
                  ]
                });
                alert.present()



              }else{
                this.loading.dismiss();
                alert = this.alertCtrl.create({
                  title: 'Aviso',
                  message: 'Lo sentimos, intentalo mas tarde',
                  buttons: [

                    {
                      text: 'Aceptar',
                      handler: () => {
                        this.nav.pop();
                      }
                    }
                  ]
                });
                alert.present()



              }

              secondaryApp.delete();
        });




    };

    resetPassword(email: string){
      return firebase.auth().sendPasswordResetEmail(email);
    }

  }

