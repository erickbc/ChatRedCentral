import { ViewCreateUsuarios } from './../pages/createUser/view-createUser';
import { UsersIncidentePage } from './../pages/usersIncidente/usersIncidente';
import { EditIncidentePage } from './../pages/editIncidente/editIncidente';
import { PushNotificationProvider } from './../providers/rest-provider/pushNotification-provider';
import { CreateIncidentePage } from './../pages/createIncidente/createIncidente';
import { InsidentePage } from './../pages/incidentePage/incidentePage';
import { LastChatPage } from './../pages/lastChats/lastChat';
import { GroupsPage } from './../pages/groups/groups';
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { AngularFireModule, AuthProviders, AuthMethods } from 'angularfire2';
import { MyApp } from './app.component';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { ChatsPage } from '../pages/chats/chats';
import { AccountPage } from '../pages/account/account';
import { ChatViewPage } from '../pages/chat-view/chat-view';

import { AuthProvider } from '../providers/auth-provider/auth-provider';
import { ChatsProvider } from '../providers/chats-provider/chats-provider';
import { UserProvider } from '../providers/user-provider/user-provider';
import { UtilProvider } from '../providers/utils';
import { CreateGroupPage } from "../pages/createGroup/createGroup";
import { ChatGroupViewPage } from "../pages/chatGroup-view/chatGroup-view";
import { SearchPage } from "../pages/search/search";
import { EditGroupPage } from "../pages/editGroup/editGroup";
import { ViewUsuarios } from '../pages/gestionUsuarios/view-usuarios';
import { ViewEditUsuario } from '../pages/editUser/view-editUser';

import { SplashScreen } from '@ionic-native/splash-screen';

export const firebaseConfig = {
  apiKey: "AIzaSyBEHjhEuZ3ovv16OkDa0pV1p_hm0l5UDAM",
  authDomain: "testfirebase-d2d5b.firebaseapp.com",
  databaseURL: "https://testfirebase-d2d5b.firebaseio.com",
  projectId: "testfirebase-d2d5b",
  storageBucket: "testfirebase-d2d5b.appspot.com",
  messagingSenderId: "373781110730"
};

const myFirebaseAuthConfig = {
  provider: AuthProviders.Password,
  method: AuthMethods.Password
}

@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    LoginPage,
    ChatsPage,
    AccountPage,
    ChatViewPage,
    GroupsPage,
    CreateGroupPage,
    ChatGroupViewPage,
    SearchPage,
    LastChatPage,
    InsidentePage,
    CreateIncidentePage,
    EditIncidentePage,
    UsersIncidentePage,
    EditGroupPage,
    ViewUsuarios,
    ViewCreateUsuarios,
    ViewEditUsuario
  ],
  imports: [
    IonicModule.forRoot(MyApp, {
      tabsHideOnSubPages: true,
    }),
    AngularFireModule.initializeApp(firebaseConfig, myFirebaseAuthConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    LoginPage,
    ChatsPage,
    AccountPage,
    ChatViewPage,
    GroupsPage,
    CreateGroupPage,
    ChatGroupViewPage,
    SearchPage,
    LastChatPage,
    InsidentePage,
    CreateIncidentePage,
    EditIncidentePage,
    UsersIncidentePage,
    EditGroupPage,
    ViewUsuarios,
    ViewCreateUsuarios,
    ViewEditUsuario

  ],
  providers: [
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    ChatsProvider,
    UserProvider,
    UtilProvider,
    PushNotificationProvider,
    Storage,
    SplashScreen]
})
export class AppModule {}
