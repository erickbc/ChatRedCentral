import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, Events } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { AngularFire } from 'angularfire2';
import { LoginPage } from '../pages/login/login';
import { TabsPage } from '../pages/tabs/tabs';
import { AuthProvider } from '../providers/auth-provider/auth-provider';
import { UserProvider } from '../providers/user-provider/user-provider';
import { ViewUsuarios } from '../pages/gestionUsuarios/view-usuarios';
import { SplashScreen } from '@ionic-native/splash-screen';




@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  kindUser:any;
  rootPage:any;
  token:any;
  @ViewChild("contenido") menu:NavController;

  ams = ViewUsuarios
  constructor(

    platform: Platform,
    public af: AngularFire,
    public userProvider: UserProvider,
    public authProvider:AuthProvider,
    public events: Events,
    private splashScreen: SplashScreen,
  ) {



    events.subscribe('username:kindUser', kind => {
      if(kind !== undefined && kind !== ""){
        console.log("==========KInd User===============")
        console.log(kind)
        this.kindUser = kind;
      }
    })



    this.kindUser = "2001";
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

      if (splashScreen) {
        setTimeout(() => {
          splashScreen.hide();
        }, 1000);
        }

      StatusBar.styleDefault();
      Splashscreen.hide();
      this.intialize();
      

    });
  }

  intialize() {


    let s = this.af.auth.subscribe(auth => {
       if(auth) {
        this.af.auth.logout();
        this.rootPage = LoginPage;
        } else {
          this.rootPage = LoginPage;

        }
    });



  }
  openMenu(){
    console.log("===========openMenu===============")
  }

  ionViewDidEnter(){
    console.log("===========ionViewDidEnter===============")
  }
  irAPagina(pagina:any){
    console.log("Ir a Pagina")
    //this.menu.setRoot(pagina);
    this.menu.push(pagina);

    //this.menuCtrl.close
  }

  cerrarSession(){
    this.af.auth.logout();
    this.rootPage = LoginPage;
    this.menu.setRoot(LoginPage)
    //this.menu.popToRoot();
  }

}
