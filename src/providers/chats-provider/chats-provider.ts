import { Content } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { AngularFire } from 'angularfire2';
import { UserProvider } from '../user-provider/user-provider';

@Injectable()
export class ChatsProvider {

    content: Content;


    constructor(public af: AngularFire, public up: UserProvider) {}



    //Obtiene de la base los issues actuales, mas recientes
    //@Validado
    getIssuesUserNow(){
        return this.af.database.list(`/users/${this.up.uid}/issues`,{
            query: {
                orderByChild: 'dateUpdate',
            }
        }).map( (arr) => { return arr; } );
    }
    //Obtiene los grupos donde estas participando, recordando que :
    //Grupos -> SubGrupo (Issue) -> Chat
    //@Validado
    getGroups() {
        return this.af.database.list(`/users/${this.up.uid}/groups/`);
    }
   //Obtiene Todos los Grupos
    //@Validado
    getAllGroups() {
      return this.af.database.list(`/groups/`);
    }
     //Obtiene Todos los Issues de un Grupo
    //@Validado
    getAllIssues() {
      return this.af.database.list("/issues/");
    }

         //Obtiene Todos los Issues de un Grupo
    //@Validado
    getIssue(keyIssue) {
      return this.af.database.object(`/issues/${keyIssue}`);
    }
    //Crea un grupo, recordando que :
    //Grupos -> SubGrupo (Issue) -> Chat
    //@Validado
    createGroup(name,users){
        let group = this.af.database.list("/groups");
        var data = {
            "name":name,
            "type":"Normal",
            "users":users,
            "lastMessage":"",
            "dateCreated":new Date().toISOString(),
            "status":true
        }
        //Se iteran los usuarios para definir a que grupo pertenecer,
        //asi como si son owners o no
        group.push(data).then((item) => {
        for (var key in users) {
            let chats = this.af.database.object(`/users/${key}/groups/${item.key}/owner`);
                chats.set(users[key]);
            let chatsStatus = this.af.database.object(`/users/${key}/groups/${item.key}/status`);
                chatsStatus.set(true);
        }
        });
    }
    //Obtiene los Issues en un grupo , en los que participa un usuario
    //@Validado
    getIssuesInGroup(group) {
        return this.af.database.list(`/users/${this.up.uid}/groups/${group}/issues/`);
    }

    //Crea un issue
    //@Validado
    createIssue(name,group){
        let issue = this.af.database.list("/issues");
        var data = {
            "name":name,
            "idGroup":group.$key,
            //"users":users,
            "dateCreated":new Date().toISOString(),
            "status":true
        }
        console.log("CREATE ISSUE");
        console.log(group.$key);


        issue.push(data).then((item) => {

         this.af.database.list(`/groups/${group.$key}/issues/`).subscribe(iss => {
            let dicIssues = []


            iss.forEach(i => {

              console.log("ISSUE INTERNO")
              dicIssues[i.$key]=i.$value
            });

            dicIssues[item.key]=true

            this.af.database.object(`/groups/${group.$key}/issues/`).set(dicIssues);
          });

        for (var key in group.users) {
            var content = {
              "owner":group.users[key],
              "status":true
            }
            let chats = this.af.database.object(`/users/${key}/groups/${group.$key}/issues/${item.key}`);
                chats.set(content);
            let issueUser = this.af.database.object(`/users/${key}/issues/${item.key}/`);
            var dataIssue = {
                "lastMessage":"",
                "idGroup":group.$key,
                "nameGroup":group.name,
                "nameIssue":name,
                "dateUpdate":new Date().toISOString(),
                "status":true
            }
            issueUser.set(dataIssue)
        }
        });

    }

    //Obtiene la lista de mensajes en el chat
    //@Validado
    getChatGroupRef(keyGroup) {
        let firstRef = this.af.database.object(`/chats/${keyGroup}`, {preserveSnapshot:true});
        let promise = new Promise((resolve, reject) => {
            firstRef.subscribe(snapshot => {
                //let a = snapshot.exists();
                resolve(`/chats/${keyGroup}`);
                if(this.content){
                    this.content.scrollToBottom();
                }
            });
        });
        return promise;
    }

    //Get Information Issue
    //@Validado
    getAllUsersIssue(key) {
        return this.af.database.object(`/issues/${key}`);
    }
      //Get Information Issue
    //@Validado
    getGroup(key) {
      return this.af.database.object(`/groups/${key}`);
    }

    //Agrega usuarios a un issue en especifico
    //@Validado
    updateUsersIssue(users,idIssue){
        let issueUsers = this.af.database.object(`/issues/${idIssue}/users`);
        issueUsers.set(users);
    }
    //Set Admin Group
    //@Validado
    setAdminGroup(keyUser,idGroup){
      let userGroup = this.af.database.object(`/groups/${idGroup}/users/${keyUser}`);
        userGroup.set(true);

      let userInGroup = this.af.database.object(`/users/${keyUser}/groups/${idGroup}/owner`);
        userInGroup.set(true);
  }
    //Agrega informacion del issue a la informacion del usuario
    //@Validado
    updateUsersIssueInformation(information,user,idIssue){

         let issue = this.af.database.object(`/users/${user}/issues/${idIssue}`);
         issue.set(information)
    }

    //Borrar un usuario de un Issue
    //@Validado
    removeUserIssue(key,issue){
        var users = this.af.database.list(`/issues/${issue}/users`);
            users.remove(key);
            //users.remove(key).then(_ => console.log('item deleted!'));

    }

    //Update Name Issue
    //@Validado
    updateIssue(name,idIssue){
       let issueName = this.af.database.object(`/issues/${idIssue}/name`);
            issueName.set(name);
    }

    //Update Name Group
    //@Validado
    saveGroup(name,idGroup){
      console.log("chats-providers.ts - saveGroup")
      console.log(name)
      console.log(idGroup)

      let issueName = this.af.database.object(`/groups/${idGroup}/name`);
           issueName.set(name);
   }

    //Get Users in Group
    //@Validado
    getAllUsersGroup(key) {
        return this.af.database.object(`/groups/${key}`);
    }

     //Get Users in Group
    //@Validado
    getListUsersGroup(key) {
      return this.af.database.list(`/groups/${key}/users`);
  }
    //Get Users in Application
    //@Validado
    getAllUsers() {
        return this.af.database.list(`/users`);
    }

    //Agrega usuarios a un grupo en especifico
    //@Validado
    updateUsersGroup(users,keyGroup){
        let issueUsers = this.af.database.object(`/groups/${keyGroup}/users`);
        issueUsers.set(users);

        for (var key in users) {
            let group = this.af.database.object(`/users/${key}/groups/${keyGroup}/owner`);
                group.set(users[key])
            let status = this.af.database.object(`/users/${key}/groups/${keyGroup}/status`);
                status.set(true)

        }
        //Need add Information to User (Particular)

    }
    //ListaUsuarios de un Grupo
    //@Validado
    getUsersGroup(idIssue){
        return this.af.database.list(`/groups/${idIssue}/users`);
    }

    //Elimina Usuario de un grupo e incidentes
    //@Validado
    deleteUserGroup(keyUser,keyGroup){
      console.log("chats-providers.ts - deleteUserGroup")
      let sub1:any;
         let query = `/users/${keyUser}/groups/${keyGroup}/issues`
         sub1 = this.af.database.list(query).subscribe(elementos => {
          elementos.forEach(element => {
            var issueObject = this.af.database.list(`/issues/${element.$key}/users/`);
            issueObject.remove(keyUser);

            this.af.database.object(`/users/${keyUser}/issues/${element.$key}`).remove();


          });
          this.af.database.object(`/users/${keyUser}/groups/${keyGroup}`).remove()
          this.af.database.object(`/groups/${keyGroup}/users/${keyUser}`).remove();
          sub1.unsubscribe();
         });
    }


    //Obtiene los issues donde esta enrolado el usuario
    //@Validado
    getIssuesRolled(){
      console.log();
      return this.af.database.list(`/users/${this.up.uid}/issues`);
    }


    //Obtiene la lista de mensajes de un chat
    //@Validado
    getChatMessages(chatId){
      console.log();
      return this.af.database.list(`/chats/${chatId}/`)
    }

    //Borra un grupo
    //@Validado
    deleteGroup(keyGroup){
      console.log("chats-providers.ts - deleteGroup")

      let sub:any;

      let issuesQuery = `/groups/${keyGroup}/issues`
      sub = this.af.database.list(issuesQuery).subscribe(issues => {
        issues.forEach(issue => {
          let i = `/issues/${issue.$key}/status`
          this.af.database.object(i).set(false)

          let iS = `/groups/${keyGroup}/issues/${issue.$key}`
          this.af.database.object(iS).set(false)
        });
        sub.unsubscribe();
      });
      let sub1:any;
      let usersInGroup = `/groups/${keyGroup}/users`
      sub1 = this.af.database.list(usersInGroup).subscribe(users => {
        users.forEach(usuario => {
          let issuesQuery = `/users/${usuario.$key}/groups/${keyGroup}/issues`
          let sub3 = this.af.database.list(issuesQuery).subscribe(issues => {
            issues.forEach(issue => {
              let i = `/users/${usuario.$key}/groups/${keyGroup}/issues/${issue.$key}/status`
                this.af.database.object(i).set(false)

              let iO = `/users/${usuario.$key}/issues/${issue.$key}/status`
                this.af.database.object(iO).set(false)
            });

            let groupQuery = `/users/${usuario.$key}/groups/${keyGroup}/status`
            this.af.database.object(groupQuery).set(false)


            let groupSingle = `/groups/${keyGroup}/status`
            this.af.database.object(groupSingle).set(false)
            sub3.unsubscribe();
          });

        });
        sub1.unsubscribe();
      });

    }

     //Borra un incidente
    //@Validado
    deleteIssue(keyGroup,keyIssue){
      console.log("chats-providers.ts - deleteGroup")

      let sub:any;

      let i = `/issues/${keyIssue}/status`
      this.af.database.object(i).set(false)

      let iS = `/groups/${keyGroup}/issues/${keyIssue}`
      this.af.database.object(iS).set(false)


      let sub1:any;
      let usersInGroup = `/groups/${keyGroup}/users`
      sub1 = this.af.database.list(usersInGroup).subscribe(users => {
        users.forEach(usuario => {

          let i = `/users/${usuario.$key}/groups/${keyGroup}/issues/${keyIssue}/status`
          this.af.database.object(i).set(false)

          let iO = `/users/${usuario.$key}/issues/${keyIssue}/status`
          this.af.database.object(iO).set(false)



        });
        sub1.unsubscribe();
      });

    }

  // get list of Chats of a Logged In User
  getChats() {
     return this.up.getUid().then(uid => {
        let chats = this.af.database.list(`/users/${uid}/chats`);
        return chats;
     });
  }


   getChatsSearch() {
     return this.up.getUid().then(uid => {
        let chats = this.af.database.list(`/users/${uid}/chats`);
        return chats;
     });
  }


  getLastIssues(){
     return this.af.database.list(`/issues`,{
                            query:{
                                orderByChild:'dateUpdate',
                            }
                        }).map((arr) => { return arr; })





  }





  getGroupIssues(key_Group){
    console.log("ID KEY GROUP")
    console.log(key_Group)
    return this.up.getUid().then(uid => {
     let groupIssues = this.af.database.list(`/users/${uid}/groups/${key_Group}/issues/`);
     return groupIssues;
    });
  }

  getUsersIssues(key_Issue){
    return this.up.getUid().then(uid => {
     let userIssues = this.af.database.list(`/issues/${key_Issue}/users`);
     return userIssues;
    });
  }


  // Add Chat References to Both users
  addChats(uid,interlocutor) {
      // First User
      let endpoint = this.af.database.object(`/users/${uid}/chats/${interlocutor}`);
      endpoint.set(true);

      // Second User
      let endpoint2 = this.af.database.object(`/users/${interlocutor}/chats/${uid}`);
      endpoint2.set(true);
  }




  getChatRef(uid, interlocutor) {
      console.log("Chat getChatRef");
      let firstRef = this.af.database.object(`/chats/${uid},${interlocutor}`, {preserveSnapshot:true});
      let promise = new Promise((resolve, reject) => {
          firstRef.subscribe(snapshot => {
                console.log("Snapshot Exists ? ");
                let a = snapshot.exists();

                if(a) {
                    console.log("Snapshot Exists");
                    resolve(`/chats/${uid},${interlocutor}`);
                } else {
                    console.log("Snapshot Not Exists!!");
                    console.log("Second Ref");
                    let secondRef = this.af.database.object(`/chats/${interlocutor},${uid}`, {preserveSnapshot:true});
                    secondRef.subscribe(snapshot => {
                        console.log("Subscribe Second Ref");
                        let b = snapshot.exists();
                        if(!b) {
                            console.log("Secoond Subscribe not exist");
                            console.log(" CHAT - ADD");
                            this.addChats(uid,interlocutor);
                        }
                    });
                    console.log(" CHAT - RESOLVE 3");
                    resolve(`/chats/${interlocutor},${uid}`);
                    console.log(" CHAT - RESOLVE 4");
                }
            });
      });

      return promise;
  }

  getGroupChatRef(uid) {
      console.log("Chat getChatRef");
      let chats = this.af.database.object(`/users/${uid}/chats`,{preserveSnapshot:true});

      //let firstRef = this.af.database.object(`/chats/${uid},${interlocutor}`, {preserveSnapshot:true});
      let promise = new Promise((resolve, reject) => {
          chats.subscribe(snapshot => {
                console.log("GET CHATS LASTS ");



            });
      });

      return promise;
  }










    deleteUsersIssue(user,idIssue){

         let issueUsers = this.af.database.list(`/issues/${idIssue}/users`);
             issueUsers.remove(user)


     }

}

