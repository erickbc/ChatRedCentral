import { Injectable } from '@angular/core';
import { AngularFire } from 'angularfire2';
import { Storage } from '@ionic/storage';
import { Camera } from 'ionic-native';

@Injectable()
export class UserProvider {

  uid:string = "";
  email:string = "";
  token:string = "";
  nickname:string = "";
  picture:any;
  kindUser:any = 2002;



  constructor(public af:AngularFire, public local:Storage) { }

  // Get Current User's UID
  getUid() {
    return this.local.get('uid');
  }

  // Create User in Firebase
  createUser() {
      let currentUserRef = this.af.database.object(`/users/${this.uid}`);
        currentUserRef.set({email: this.email});
  }

  // Create User in Firebase
  updateUser(key,kind,nickname) {
      let currentUserRef = this.af.database.object(`/users/${key}/type`);
        currentUserRef.set(kind);
      let currentUserRef2 = this.af.database.object(`/users/${key}/nickname`);
      currentUserRef2.set(nickname);

  }


  createUserEmail(id,email,type,nickname) {
    let t = 0
    if(type=="admin"){
      t=2001 //admin
    }else{
      t=2002 //normal
    }
    let currentUserRef = this.af.database.object(`/users/${id}`);
      currentUserRef.set({email: email,status: true,type: t,nickname:nickname});


}

  activateUser(key){
    let currentUserRef = this.af.database.object(`/users/${key}/status`);
      currentUserRef.set(true);
  }
  //Retorna infromacion del usuario
    getUserInfo(uid) {
      return  this.af.database.object(`/users/${uid}`);
  }



  // Get Info of Single User
  getUser(idUser) {
    // Getting UID of Logged In User
      return this.af.database.object(`/users/${idUser}`);
  }
  // Get All Users of App
  getAllUsers() {
      return this.af.database.list('/users');
  }
  //Disable User
  disableUser(key) {
    let user = this.af.database.object(`/users/${key}/status`);
      user.set(false);
      this.deleteUserAllGroup(key)

      this.af.database.object(`/users/${key}/issues/`).remove();
      this.af.database.object(`/users/${key}/groups/`).remove();


  }

  //Borra todos los chats, grupos de un usuario (para darlo de baja)
  //@Validado
  deleteUserAllGroup(keyUser){
    let sub:any;
    console.log("chats-providers.ts - deleteUserAllGroup")
    let queryAllGroups = `/users/${keyUser}/groups/`
    sub = this.af.database.list(queryAllGroups).subscribe(grupos => {
      grupos.forEach(grupo => {
        let sub1:any;
        let query = `/users/${keyUser}/groups/${grupo.$key}/issues`
        sub1 = this.af.database.list(query).subscribe(elementos => {
          elementos.forEach(element => {
            var issueObject = this.af.database.list(`/issues/${element.$key}/users/`);
            issueObject.remove(keyUser);
            this.af.database.object(`/users/${keyUser}/issues/${element.$key}`).remove();
          });
          this.af.database.object(`/users/${keyUser}/groups/${grupo.$key}`).remove()
          this.af.database.object(`/groups/${grupo.$key}/users/${keyUser}`).remove();
          sub1.unsubscribe();
        });
      });
      sub.unsubscribe();
    });
  }


  //Disable User
  enableUser(key) {
    let user = this.af.database.object(`/users/${key}/status`);
      user.set(true);
  }
  getAllUsersInGroup(group) {
      return this.af.database.list(`/groups/${group}/users`);
  }



  // Get base64 Picture of User
  getPicture() {
      let base64Picture;
      let options = {
          destinationType: 0,
          encodingType:0,
          sourceType: 1
      };

      let promise = new Promise((resolve, reject) => {
           Camera.getPicture(options).then((imageData) => {
                base64Picture = "data:image/jpeg;base64," + imageData;
                resolve(base64Picture);
            }, (error) => {
                reject(error);
          });

      });
      return promise;
  }

  // Update Provide Picture of User
  updatePicture(image) {

    let pictureRef = this.af.database.object(`/users/${this.uid}/picture`);
        pictureRef.set(image)

  }
}

